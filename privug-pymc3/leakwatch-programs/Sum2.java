import bham.leakwatch.LeakWatchAPI;
import java.security.SecureRandom;
public class Sum2 {
public static void main(String[] args) {
    SecureRandom csprng = new SecureRandom();
    int domain = 10000;
    int x = csprng.nextInt(domain);
    int y = csprng.nextInt(domain); 
    LeakWatchAPI.secret("x", x);
    int o = x+y;
    LeakWatchAPI.observe(o);
  }
}
