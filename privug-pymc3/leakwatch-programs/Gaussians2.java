import bham.leakwatch.LeakWatchAPI;
import java.security.SecureRandom;
public class Gaussians2 {
public static void main(String[] args) {
    SecureRandom csprng = new SecureRandom();
    double x = csprng.nextGaussian()*8 + 42;
    double y = csprng.nextGaussian() + 55; 
    LeakWatchAPI.secret("x", x);
    double o = (x+y)/2;
    LeakWatchAPI.observe(o);
  }
}
