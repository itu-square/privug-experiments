import bham.leakwatch.LeakWatchAPI;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.stream.DoubleStream;
public class GaussiansN {
public static void main(String[] args) {
    SecureRandom csprng = new SecureRandom();
    double x = csprng.nextGaussian()*8 + 42; // Var[X*8] = 9**2 * Var[X] with Var[X]=1 in nextGaussian()
    LeakWatchAPI.secret("x", x);


    int N = 10000;
    double[] y = new double[N];
    DoubleStream y_stream = Arrays.stream(y);
    double o = y_stream.map(i -> {double temp=csprng.nextGaussian() + 55;
	                          LeakWatchAPI.secret("s",temp);
				  return temp;})
	               .reduce(0,(a,b)->a+b) + x;
    LeakWatchAPI.observe(o/(N+1));
  }
}
