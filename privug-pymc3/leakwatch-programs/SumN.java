import bham.leakwatch.LeakWatchAPI;
import java.security.SecureRandom;
import java.util.Arrays;
import java.util.stream.IntStream;
public class SumN {
public static void main(String[] args) {
    
    SecureRandom csprng = new SecureRandom();
    
    int num_secrets = 10000;
    int x = csprng.nextInt(num_secrets);
    LeakWatchAPI.secret("x", x);

    int N = 1;
    int[] y = new int[N];
    IntStream y_stream = Arrays.stream(y);
    int o = y_stream.map(i -> csprng.nextInt(num_secrets))
	            .reduce(0,(a,b)->a+b) + x;
    LeakWatchAPI.observe(o);
  }
}
