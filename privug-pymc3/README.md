# Implementation of the Privug method in PyMC3

Implementation of the privug method [1] in the probabilistic
programming framework [PyMC3](https://docs.pymc.io/).

The repository contains the following experiments:

- `evaluation-discrete-random-variables.ipynb` contains the
  convergence and scalability evaluation of systems consisting of
  discrete random variables.
- `evaluation-continuous-random-variables.ipynb` contains the
  convergence and scalability evaluation of systems consisting of
  continuous random variables.
- `evaluation-comparison-leakwatch.ipynb` contains convergence and
  scalability comparison of privug and
  [LeakWatch](https://www.cs.bham.ac.uk/research/projects/infotools/leakwatch/).
- `evaluation-differential-privacy.ipynb` contains the analysis of
  differential privacy system.
- `opendp-privug.ipynb` contains an example of applying privug for
  analyzing an anonymization program using a 3rd party
  library. Concretely, it uses the [OpenDP](https://github.com/opendp)
  library to add Laplacian noise to the result of the program.


## References

- [1] Raúl Pardo, Willard Rafnsson, Christian Probs and Andrzej
  Waswoski. Privug: Quantifying Leakage using Probabilistic
  Programming for Privacy Risk Analysis. arXiv preprint
  arXiv:2011.08742 (2020). https://arxiv.org/abs/2011.08742
  
