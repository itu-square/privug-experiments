name := "privug-experiments"

version := "1.0"

scalaVersion := "2.12.10"

// https://mvnrepository.com/artifact/com.cra.figaro/figaro
libraryDependencies += "com.cra.figaro" %% "figaro" % "5.0.0.0"
val scalacheckVersion = "1.14"
libraryDependencies += "org.scalacheck" %% "scalacheck" % s"$scalacheckVersion.0" % "test"
libraryDependencies += "org.scalatest" %% "scalatest" % "3.0.5" % "test"
libraryDependencies += "org.plotly-scala" %% "plotly-render" % "0.7.2"

// Run Experiment
// Set main class to: Governor, GovernorKAnonymity, AverageAge or DP for the corresponding experiment
val mainClassName: String = "Governor" // Main class to run or "compile into jar"
mainClass in (Compile, run) := Some(mainClassName)
mainClass in assembly := Some(mainClassName)
