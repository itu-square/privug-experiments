import com.cra.figaro.language._
import com.cra.figaro.library.atomic._
import com.cra.figaro.algorithm.sampling._

object ParallelSamplingTest {

  def main(args: Array[String]) = {

    val numThreads: Int = 8

    def gen(obs: Double*) = () => {
      val universe = Universe.createNew()
      val x = discrete.Uniform((1 to 100:_*))("x", universe)
      val y = discrete.Uniform((1 to 100:_*))("y", universe)

      val s = Select(0.5 -> 0.3, 0.5 -> 0.6)
      val f = Flip(s)("f", universe)
      for (o <- obs) {
        s.observe(o)
      }
      universe
    }

    val t_init = System.nanoTime()

    val i1 = Importance.par(gen(0.3), numThreads, 100000000, "f", "x", "y")
    i1.start()
    println("Probability: " + i1.probability("f", true))
    println("Probability: " + i1.probability("x", (t: Int) => (t == 2)))
    println("Probability: " + i1.probability("y", (t: Int) => (t == 2)))

    i1.kill()
    println("Execution time (ns): " + (System.nanoTime()-t_init))
  }

}
