import com.cra.figaro.language._
import com.cra.figaro.algorithm.factored.VariableElimination
import com.cra.figaro.algorithm.sampling.{Importance,MetropolisHastings => MH}
import com.cra.figaro.algorithm.sampling.parallel.ParImportance
import com.cra.figaro.library.compound._
import com.cra.figaro.library.atomic.discrete
import com.cra.figaro.library.atomic.discrete._
import com.cra.figaro.library.atomic.continuous.{Normal}
import com.cra.figaro.library.collection.{ContainerElement,
  Container,
  VariableSizeArray,
  FixedSizeArray,
  FixedSizeArrayElement,
  MakeArray}

import plotly._, layout._, Plotly._

object GovernorKAnonymity {

  // Set as strings for simplicity in masking
  type Name = GroundDataKAnonymity.Name
  type Zip = GroundDataKAnonymity.ZIP
  type BDay = GroundDataKAnonymity.Age
  type Sex = String
  type Diagnosis = String

  // Governor's info
  val GOVNAME: Name = "Governor"
  val GOVBDAY: BDay = "20"
  val GOVZIPCODE: Zip = "2300"
  val GOVSEX: Sex = "female"

  object Deterministic {

    def alpha (records: List[(Name,Zip, BDay, Sex, Diagnosis)]):
        List[(Zip, BDay, Sex, Diagnosis)] =
      records map { case (n,z,b,s,d) => (z,b,s,d) }

  }

  object Probabilistic {

    // Naive anonymization
    def alpha  (records: FixedSizeArrayElement[(Name,Zip,BDay,Sex,Diagnosis)]):
        ContainerElement[Int,(Zip, BDay, Sex, Diagnosis)] =
      records map { case (n,z,b,s,d) => (z,b,s,d) }


    // Assumption: the lenght of recods is strictly larger than k
    def alpha_k_anonymity_custom(records: ContainerElement[Int,(Zip, BDay, Sex, Diagnosis)], k: Int):
        FixedSizeArrayElement[(Zip, BDay, Sex, Diagnosis)] = {
      // Distinct values
      val distinctAges:      List[BDay]      = GroundDataKAnonymity.ages.toList.distinct ++ List[String]("*")
      val distinctSexes:     List[Sex]       = List[Sex]("male","female").distinct ++ List[String]("*")
      val distinctDiagnoses: List[Diagnosis] = List[Diagnosis]("ill","healthy").distinct ++ List[String]("*")
      val distinctZips:      List[Zip]       = GroundDataKAnonymity.zips.toList.distinct ++ List[String]("*")

      // Single attributes
      // ⋁ᵢ i where i ∈  Kz = { count(z) | z ∈ Zip ∪ {*}}
      val zip_not_k_anonymous: Element[Boolean] =
        distinctZips.map{  (zip: Zip)  => records.count(r => r._1 == zip)
                                                 .map{ (i: Int) => !(i==0 || i>=k)} }
                    .reduce((a: Element[Boolean], b: Element[Boolean]) => a||b)

      val age_not_k_anonymous: Element[Boolean] =
        distinctAges.map{ (age: BDay) => records.count(r => r._2 == age)
                                                .map{(i: Int) => !(i==0 || i>=k)} }
                    .reduce((a: Element[Boolean], b: Element[Boolean]) => a||b)

      val sex_not_k_anonymous: Element[Boolean] =
        distinctSexes.map{ (sex: Sex) => records.count(r => r._3 == sex).map{(i: Int) => !(i==0 || i>=k)} }
          .reduce((a: Element[Boolean], b: Element[Boolean]) => a||b)


      val k_ind_records: FixedSizeArrayElement[(Zip,BDay,Sex,Diagnosis)] =
        VariableSizeArray (records.length, i => for {
          z <- If(zip_not_k_anonymous, Constant("*"), records(i)._1)
          b <- If(age_not_k_anonymous, Constant("*"), records(i)._2)
          s <- If(sex_not_k_anonymous, Constant("*"), records(i)._3)
          d <- records(i)._4
            } yield (z,b,s,d)
        )


      // Combinations of 2 attributes
      val zip_sex_not_k_anonymous: Element[Boolean] =
        distinctZips.map{  (zip: Zip)  => distinctSexes.map { (sex: Sex) =>
          k_ind_records.count(r => r._1 == zip && r._3 == sex).map{ (i: Int) => !(i==0 || i>=k)}
        }.reduce((a: Element[Boolean], b: Element[Boolean]) => a||b)}.reduce((a: Element[Boolean], b: Element[Boolean]) => a||b)

      val zip_age_not_k_anonymous: Element[Boolean] =
        distinctZips.map{  (zip: Zip)  => distinctAges.map { (age: BDay) =>
          k_ind_records.count(r => r._1 == zip && r._2 == age).map{ (i: Int) => !(i==0 || i>=k)}
        }.reduce((a: Element[Boolean], b: Element[Boolean]) => a||b)}.reduce((a: Element[Boolean], b: Element[Boolean]) => a||b)

      val age_sex_not_k_anonymous: Element[Boolean] =
        distinctAges.map{  (age: BDay)  => distinctSexes.map { (sex: Sex) =>
          k_ind_records.count(r => r._2 == age && r._3 == sex).map{ (i: Int) => !(i==0 || i>=k)}
        }.reduce((a: Element[Boolean], b: Element[Boolean]) => a||b)}.reduce((a: Element[Boolean], b: Element[Boolean]) => a||b)

      val k_ind_pairs_records: FixedSizeArrayElement[(Zip,BDay,Sex,Diagnosis)] =
        VariableSizeArray (records.length, i => for {
          z <- If(zip_sex_not_k_anonymous||zip_age_not_k_anonymous, Constant("*"), k_ind_records(i)._1)
          b <- If(zip_age_not_k_anonymous||age_sex_not_k_anonymous, Constant("*"), k_ind_records(i)._2)
          s <- If(zip_sex_not_k_anonymous||age_sex_not_k_anonymous, Constant("*"), k_ind_records(i)._3)
          d <- k_ind_records(i)._4
            } yield (z,b,s,d)
        )

      // Combinations of 3 attributes
      val zip_age_sex_not_k_anonymous: Element[Boolean] =
        distinctAges.map{  (age: BDay)  => distinctSexes.map { (sex: Sex) => distinctZips.map { (zip: Zip) => 
          k_ind_pairs_records.count(r => r._1 == zip && r._2 == age && r._3 == sex).map{ (i: Int) => !(i==0 || i>=k)}
        }.reduce((a: Element[Boolean], b: Element[Boolean]) => a||b)}
         .reduce((a: Element[Boolean], b: Element[Boolean]) => a||b)}
         .reduce((a: Element[Boolean], b: Element[Boolean]) => a||b)

      val k_ind_triples_records: FixedSizeArrayElement[(Zip,BDay,Sex,Diagnosis)] =
        VariableSizeArray (records.length, i => for {
          z <- If(zip_age_sex_not_k_anonymous, Constant("*"), k_ind_pairs_records(i)._1)
          b <- If(zip_age_sex_not_k_anonymous, Constant("*"), k_ind_pairs_records(i)._2)
          s <- If(zip_age_sex_not_k_anonymous, Constant("*"), k_ind_pairs_records(i)._3)
          d <- k_ind_pairs_records(i)._4
            } yield (z,b,s,d)
        )

      return k_ind_triples_records
    }

    // A prior
    def prior(): FixedSizeArrayElement[(Name,Zip,BDay,Sex,Diagnosis)] = {

      // Database of constant size N
      val N: Int = 500
      val size: Element[Int] = Constant(N)

      // First row (i==0) includes governor's data
      // Remaining rows distributions modeling the assumptions of the datasets
      // We could remove the first row and add conditions (too slow for sampler)
      VariableSizeArray (size, i => for {
        n <- if (i==0) Constant(GOVNAME)    else Uniform(GroundDataKAnonymity.names:_*)
        z <- if (i==0) Constant(GOVZIPCODE) else Uniform(GroundDataKAnonymity.zips:_*)
        b <- if (i==0) Constant(GOVBDAY)    else Uniform(GroundDataKAnonymity.ages:_*)
        s <- if (i==0) Constant(GOVSEX)     else Uniform("male", "female")
        d <- if (i==0) Constant("ill")      else If(Flip(.2), "ill", "healthy")
      } yield (n,z,b,s,d) )
    }

  } // Probabilistic

  def maskN(n: Int, zip: String): String = {zip.dropRight(zip.length()-n)+"*"*(zip.length()-n)}
  def computeHierarchyZIPs(zip: String): List[String] = {
    val n: Int = zip.length()
    val l: List[String] = (0 to n).toList.map{(x: Int) => x.toString()}
    return l.map{(x: String) => maskN(x.toInt,zip)}.reverse
  }
  def computeHierarchySexes(sex: String): List[String] = {
    List[String](sex,"*")
  }
  def computeHierarchyAges(age: String): List[String] = age match {
    case age if age.toInt < 50 => List[String](age,"<50","*")
    case age if age.toInt >= 50 => List[String](age,">=50","*")
  }
  def computeHierarchyDiagnosis(diagnosis: String): List[String] = {
    List[String](diagnosis,"*")
  }

  // Test for non-probabilistic k-anonymity
  def testRecords: List[(Name,Zip,BDay,Sex,Diagnosis)] = {
    List[(Name,Zip,BDay,Sex,Diagnosis)](
      ("alice","2300","31","female","healthy"),
      ("bob","2680","51","female","healthy"),
      ("charlie","2800","11","female","healthy"),
      ("dain","3320","61","female","ill"),
      ("edward","3550","11","male","healthy"),
      ("francis","2950","11","male","healthy"),
      ("governor",GOVZIPCODE,GOVBDAY,GOVSEX,"ill"),
    )
  }

  // Non-probabilistic k-anonymity
  def alpha_k_anonymity_custom(records: List[(Zip,BDay,Sex,Diagnosis)], k: Int): List[(Zip,BDay,Sex,Diagnosis)] = { 
    // Distinct values
    val distinctAges:      List[BDay]      = GroundDataKAnonymity.ages.toList.distinct
    val distinctSexes:     List[Sex]       = List[Sex]("male","female").distinct
    val distinctDiagnoses: List[Diagnosis] = List[Diagnosis]("ill","healthy").distinct
    val distinctZips:      List[Zip]       = GroundDataKAnonymity.zips.toList.distinct

    // Little trick because I cannot write >= for Element[Int] objects
    val bad_ks = 1 to k-1

    // Anonymize zips
    val zip_not_k_anonymous: Boolean = distinctZips.map{ zip => {
      val i: Int = records.count(r => r._1 == zip)
      !(i==0 || i>=k)
    }}.reduce((a: Boolean, b: Boolean) => a||b)
    if (zip_not_k_anonymous) {
      return alpha_k_anonymity_custom(records.map{ case (z,b,s,d) => ("*",b,s,d) },k)
    }

    // Anonymize bdays
    val age_not_k_anonymous: Boolean = distinctAges.map{ age => {
      val i: Int = records.count(r => r._2 == age)
      !(i==0 || i>=k)
    }}.reduce((a: Boolean, b: Boolean) => a||b)
    if (age_not_k_anonymous) {
      return alpha_k_anonymity_custom(records.map{ case (z,b,s,d) => (z,"*",s,d) },k)
    }

    // Anonymize sexes
    val sex_not_k_anonymous: Boolean = distinctSexes.map{ sex => {
      val i: Int = records.count(r => r._3 == sex)
      !(i==0 || i>=k)
    }}.reduce((a: Boolean, b: Boolean) => a||b)    
    if (sex_not_k_anonymous) {
      return alpha_k_anonymity_custom(records.map{ case (z,b,s,d) => (z,"*",s,d) },k)
    }

    return records
  }

  def main(args: Array[String]) = {
    // Non-probabilistic execution
    println(alpha_k_anonymity_custom(Deterministic.alpha(testRecords),2))


    val prior: FixedSizeArrayElement[(Name,Zip,BDay,Sex,Diagnosis)] = Probabilistic.prior()    
    val posterior: FixedSizeArrayElement[(Zip,BDay,Sex,Diagnosis)] = Probabilistic.alpha_k_anonymity_custom(Probabilistic.alpha(prior),2)


    //************Queries************//
    // P(∀ n ∈ (1,N) · zₙ = GOVZIPCODE → dₙ=Ill) where N is the size of the dataset
    val governorIllZip: Element[Boolean]        = posterior forall {
      case (z,b,s,d) =>
        if (z == GOVZIPCODE) d == "ill"
        else d=="ill" || d=="healthy" // always true
    }

    val governorIllBday: Element[Boolean]       = posterior forall {
      case (z,b,s,d) =>
        if (b == GOVBDAY) d == "ill"
        else true
    }
    val governorIllSex: Element[Boolean]        = posterior forall {
      case (z,b,s,d) =>
        if (s == GOVSEX) d == "ill"
        else true
    }
    val governorIllZipBday: Element[Boolean]    = posterior forall {
      case (z,b,s,d) =>
        if (z == GOVZIPCODE && b == GOVBDAY) d == "ill"
        else true
    }
    val governorIllZipSex: Element[Boolean]     = posterior forall {
      case (z,b,s,d) =>
        if (z == GOVZIPCODE && s == GOVSEX) d == "ill"
        else true
    }
    val governorIllBdaySex: Element[Boolean]    = posterior forall {
      case (z,b,s,d) =>
        if (b == GOVBDAY && s == GOVSEX) d == "ill"
        else true
    }
    val governorIllZipBdaySex: Element[Boolean] = posterior forall {
      case (z,b,s,d) =>
        if (z == GOVZIPCODE && b == GOVBDAY && s == GOVSEX) d == "ill"
        else true
    }    
    //************Queries************//

    //******Additional Queries*******//
    val governorZipIsInDataset: Element[Boolean] = posterior exists {
      case (z,b,s,d) => z == GOVZIPCODE
    }
    val zipPresentAndIll: Element[Boolean] = governorIllZip && governorZipIsInDataset
    //******Additional Queries*******//



    //*********Attributes uniqueness queries******//
    val numGovZip: Element[Int]        = posterior count {
      case (z,b,s,d) =>
        z == GOVZIPCODE
    }
    val numGovBday: Element[Int]       = posterior count {
      case (z,b,s,d) =>
        b == GOVBDAY
    }
    val numGovSex: Element[Int]        = posterior count {
      case (z,b,s,d) =>
        s == GOVSEX
    }
    val numGovZipBday: Element[Int]    = posterior count {
      case (z,b,s,d) =>
        z == GOVZIPCODE &&
        b == GOVBDAY
    }
    val numGovZipSex: Element[Int]     = posterior count {
      case (z,b,s,d) =>
        z == GOVZIPCODE &&
        s == GOVSEX
    }
    val numGovBdaySex: Element[Int]    = posterior count {
      case (z,b,s,d) =>
        s == GOVSEX &&
        b == GOVBDAY
    }
    val numGovZipBdaySex: Element[Int] = posterior count {
      case (z,b,s,d) =>
        z == GOVZIPCODE &&
        b == GOVBDAY &&
        s == GOVSEX
    }
    //*********Attributes uniqueness queries******//


    val alg = Importance(1000, // Warning: very long execution time!
      //Queries
      governorIllZip,governorIllBday,governorIllSex,
      governorIllZipSex,governorIllZipBday,governorIllBdaySex,
      governorIllZipBdaySex,
      //Attribute Uniqueness queries
      numGovZip,numGovBday,numGovSex,
      numGovZipSex,numGovZipBday,numGovBdaySex,
      numGovZipBdaySex,
      //Probability of learning diagnosis
      zipPresentAndIll
    )
    alg.start()


    println("Zip Present and Ill: " + alg.probability(zipPresentAndIll, (x: Boolean) => x))

    println("\n\n//************Queries************//")
    println("P(∀ n ∈  (1,N) · zₙ = GOVZIPCODE →  dₙ=Ill)                               = " +
      alg.probability(governorIllZip, (x: Boolean) => x))
    println("P(∀ n ∈  (1,N) · bₙ = GOVBDAY    →  dₙ=Ill)                               = " +
      alg.probability(governorIllBday, (x: Boolean) => x))
    println("P(∀ n ∈  (1,N) · sₙ = GOVSEX     →  dₙ=Ill)                               = " +
      alg.probability(governorIllSex, (x: Boolean) => x))
    println("P(∀ n ∈  (1,N) · zₙ = GOVZIPCODE ∧ bₙ = GOVBDAY  →  dₙ=Ill)               = " +
      alg.probability(governorIllZipBday, (x: Boolean) => x))
    println("P(∀ n ∈  (1,N) · zₙ = GOVZIPCODE ∧ sₙ = GOVSEX   →  dₙ=Ill)               = " +
      alg.probability(governorIllZipSex, (x: Boolean) => x))
    println("P(∀ n ∈  (1,N) · bₙ = GOVBDAY    ∧ sₙ = GOVSEX   →  dₙ=Ill)               = " +
      alg.probability(governorIllBdaySex, (x: Boolean) => x))
    println("P(∀ n ∈  (1,N) · zₙ = GOVZIPCODE ∧ bₙ = GOVBDAY ∧ sₙ = GOVSEX  →  dₙ=Ill) = " +
      alg.probability(governorIllZipBdaySex, (x: Boolean) => x))


    println("\n\n//************Attribute Uniqueness Plots************//")
    PlotFigaro.plotElementIntProbabilities(alg,numGovZip,
      1, (0,50),
      "Number of rows with Governor's ZIP",
      "numgovzip.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovBday,
      1, (0,50),
      "Number of rows with Governor's BDay",
      "numgovbday.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovSex,
      1, (0,1000),
      "Number of rows with Governor's Sex",
      "numgovsex.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovZipSex,
      1, (0,50),
      "Number of rows with Governor's Zip and Sex",
      "numgovzipsex.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovZipBday,
      1, (0,50),
      "Number of rows with Governor's Zip and BDay",
      "numgovzipbday.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovBdaySex,
      1, (0,50),
      "Number of rows with Governor's BDay & Sex",
      "numgovbdaysex.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovZipBdaySex,
      1, (0,50),
      "Number of rows with Governor's Zip & BDay & Sex",
      "numgovzipbdaysex.html")

    alg.kill()
  }
}
