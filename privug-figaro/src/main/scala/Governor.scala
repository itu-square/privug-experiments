import com.cra.figaro.language._
import com.cra.figaro.algorithm.factored.VariableElimination
import com.cra.figaro.algorithm.sampling.{Importance,MetropolisHastings => MH}
import com.cra.figaro.algorithm.sampling.parallel.ParImportance
import com.cra.figaro.library.compound._
import com.cra.figaro.library.atomic.discrete
import com.cra.figaro.library.atomic.discrete._
import com.cra.figaro.library.atomic.continuous.{Normal}
import com.cra.figaro.library.collection.{ContainerElement,
  VariableSizeArray,
  FixedSizeArray,
  FixedSizeArrayElement,
  MakeArray}

import plotly._, layout._, Plotly._

object Governor {

  type Name = GroundData.Name
  type Zip = GroundData.ZIP
  type BDay = Int // to keep things simple for the time being

  sealed trait Sex
  case object Male extends Sex
  case object Female extends Sex

  sealed trait Diagnosis
  case object Healthy extends Diagnosis
  case object Ill extends Diagnosis

  // Governor's info
  val GOVNAME: GroundData.Name = "Governor"
  val GOVBDAY: BDay = 20
  val GOVZIPCODE: GroundData.ZIP = 2300
  val GOVSEX: Sex = Female

  object Deterministic {

    def alpha (records: List[(Name,Zip, BDay, Sex, Diagnosis)]):
        List[(Zip, BDay, Sex, Diagnosis)] =
      records map { case (n,z,b,s,d) => (z,b,s,d) }

  }

  object Probabilistic {

   def alpha  (records: FixedSizeArrayElement[(Name,Zip,BDay,Sex,Diagnosis)]):
       ContainerElement[Int,(Zip, BDay, Sex, Diagnosis)] =
      records map { case (n,z,b,s,d) => (z,b,s,d) }

    // A prior
    def prior(): FixedSizeArrayElement[(Name,Zip,BDay,Sex,Diagnosis)] = {

      // Database of constant size N
      val N: Int = 1000
      val size: Element[Int] = Constant(N)

      // First row (i==0) includes governor's data
      // Remaining rows distributions modeling the assumptions of the datasets
      // We could remove the first row and add conditions (too slow for sampler)
      VariableSizeArray (size, i => for {
        n <- if (i==0) Constant(GOVNAME)    else Uniform(GroundData.names:_*)
        z <- if (i==0) Constant(GOVZIPCODE) else Uniform(GroundData.zips:_*)
        b <- if (i==0) Constant(GOVBDAY)    else FromRange(0, 100)
        s <- if (i==0) Constant(GOVSEX)     else Uniform(Male, Female)
        d <- if (i==0) Constant(Ill)        else If(Flip(.2), Ill, Healthy)
      } yield (n,z,b,s,d) )
    }

  } // Probabilistic


  def main(args: Array[String]) = {

    val prior: FixedSizeArrayElement[(Name,Zip,BDay,Sex,Diagnosis)] = Probabilistic.prior()
    val posterior: ContainerElement[Int,(Zip,BDay,Sex,Diagnosis)]   = Probabilistic.alpha(prior)

    
    //************Queries************//
    // P(∀ n ∈ (1,N) · zₙ = GOVZIPCODE → dₙ=Ill) where N is the size of the dataset
    val governorIllZip: Element[Boolean]        = posterior forall {
      case (z,b,s,d) =>
        if (z == GOVZIPCODE) d == Ill
        else d==Ill || d==Healthy // always true
    }
    val governorIllBday: Element[Boolean]       = posterior forall {
      case (z,b,s,d) =>
        if (b == GOVBDAY) d == Ill
        else true
    }
    val governorIllSex: Element[Boolean]        = posterior forall {
      case (z,b,s,d) =>
        if (s == GOVSEX) d == Ill
        else true
    }
    val governorIllZipBday: Element[Boolean]    = posterior forall {
      case (z,b,s,d) =>
        if (z == GOVZIPCODE && b == GOVBDAY) d == Ill
        else true
    }
    val governorIllZipSex: Element[Boolean]     = posterior forall {
      case (z,b,s,d) =>
        if (z == GOVZIPCODE && s == GOVSEX) d == Ill
        else true
    }
    val governorIllBdaySex: Element[Boolean]    = posterior forall {
      case (z,b,s,d) =>
        if (b == GOVBDAY && s == GOVSEX) d == Ill
        else true
    }
    val governorIllZipBdaySex: Element[Boolean] = posterior forall {
      case (z,b,s,d) =>
        if (z == GOVZIPCODE && b == GOVBDAY && s == GOVSEX) d == Ill
        else true
    }
    //************Queries************//



    //*********Sanity checks*********//
    val governorIllPrior: Element[Boolean] = prior exists {
      case (n,z,b,s,d) =>
        n == GOVNAME &&
        d == Ill
    }
    val firstRowDiagnosis: Element[Diagnosis]    = posterior(0)._4    
    //*********Sanity checks*********//

    val secondRowZip:  Element[Int]   = posterior(1)._1
    val secondRowBDay:    Element[Int]   = posterior(1)._2
    // val secondRowSex: Element[Int]  = posterior(1)._3
    



    //*********Attributes uniqueness queries******//
    val numGovZip: Element[Int]        = posterior count {
      case (z,b,s,d) =>
        z == GOVZIPCODE
    }
    val numGovBday: Element[Int]       = posterior count {
      case (z,b,s,d) =>
        b == GOVBDAY
    }
    val numGovSex: Element[Int]        = posterior count {
      case (z,b,s,d) =>
        s == GOVSEX
    }
    val numGovZipBday: Element[Int]    = posterior count {
      case (z,b,s,d) =>
        z == GOVZIPCODE &&
        b == GOVBDAY
    }
    val numGovZipSex: Element[Int]     = posterior count {
      case (z,b,s,d) =>
        z == GOVZIPCODE &&
        s == GOVSEX
    }
    val numGovBdaySex: Element[Int]    = posterior count {
      case (z,b,s,d) =>
        s == GOVSEX &&
        b == GOVBDAY
    }
    val numGovZipBdaySex: Element[Int] = posterior count {
      case (z,b,s,d) =>
        z == GOVZIPCODE &&
        b == GOVBDAY &&
        s == GOVSEX
    }
    //*********Attributes uniqueness queries******//


    //*********Attributes uniqueness queries******//
    val numGovIllZip: Element[Int]        = posterior count {
      case (z,b,s,d) =>
        z == GOVZIPCODE &&
        d == Ill
    }
    val numGovIllBday: Element[Int]       = posterior count {
      case (z,b,s,d) =>
        b == GOVBDAY &&
        d == Ill
    }
    val numGovIllSex: Element[Int]        = posterior count {
      case (z,b,s,d) =>
        s == GOVSEX &&
        d == Ill
    }
    val numGovIllZipBday: Element[Int]    = posterior count {
      case (z,b,s,d) =>
        z == GOVZIPCODE &&
        b == GOVBDAY &&
        d == Ill
    }
    val numGovIllZipSex: Element[Int]     = posterior count {
      case (z,b,s,d) =>
        z == GOVZIPCODE &&
        s == GOVSEX &&
        d == Ill
    }
    val numGovIllBdaySex: Element[Int]    = posterior count {
      case (z,b,s,d) =>
        s == GOVSEX &&
        b == GOVBDAY &&
        d == Ill
    }
    val numGovIllZipBdaySex: Element[Int] = posterior count {
      case (z,b,s,d) =>
        z == GOVZIPCODE &&
        b == GOVBDAY &&
        s == GOVSEX &&
        d == Ill
    }
    //*********Attributes uniqueness queries******//

    // // Activate the condition for probability learning queries
    // posterior count {
    //   case (z,b,s,d) =>
    //     z == GOVZIPCODE
    // } setCondition ((x: Int) => x == 5) // There are five people with the Governor's ZIP

    val alg = Importance(10000,
      // Sanity Checks
      governorIllPrior, firstRowDiagnosis,
      //Queries
      governorIllZip,governorIllBday,governorIllSex,
      governorIllZipSex,governorIllZipBday,governorIllBdaySex,
      governorIllZipBdaySex,
      //Attribute Uniqueness queries
      numGovZip,numGovBday,numGovSex,
      numGovZipSex,numGovZipBday,numGovBdaySex,
      numGovZipBdaySex,
      //Probability of learning diagnosis (activate condition for these analyses)
      numGovIllZip,numGovIllBday,numGovIllSex,
      numGovIllZipSex,numGovIllZipBday,numGovIllBdaySex,
      numGovIllZipBdaySex,
      //Inferred trace diagnosis
      secondRowBDay//,secondRowZip,secondRowSex
    )
    alg.start()

    println("\n\n//************Sanity checks************//")
    println("governor illness prior: " +
      alg.probability(governorIllPrior, (x: Boolean) => x))
    println("Element in first row (governor) illness: " +
      alg.probability(firstRowDiagnosis, (x: Diagnosis) => x==Ill))


    println("\n\n//************Queries************//")
    println("P(∀ n ∈  (1,N) · zₙ = GOVZIPCODE →  dₙ=Ill)                               = " +
      alg.probability(governorIllZip, (x: Boolean) => x))
    println("P(∀ n ∈  (1,N) · bₙ = GOVBDAY    →  dₙ=Ill)                               = " +
      alg.probability(governorIllBday, (x: Boolean) => x))
    println("P(∀ n ∈  (1,N) · sₙ = GOVSEX     →  dₙ=Ill)                               = " +
      alg.probability(governorIllSex, (x: Boolean) => x))
    println("P(∀ n ∈  (1,N) · zₙ = GOVZIPCODE ∧ bₙ = GOVBDAY  →  dₙ=Ill)               = " +
      alg.probability(governorIllZipBday, (x: Boolean) => x))
    println("P(∀ n ∈  (1,N) · zₙ = GOVZIPCODE ∧ sₙ = GOVSEX   →  dₙ=Ill)               = " +
      alg.probability(governorIllZipSex, (x: Boolean) => x))
    println("P(∀ n ∈  (1,N) · bₙ = GOVBDAY    ∧ sₙ = GOVSEX   →  dₙ=Ill)               = " +
      alg.probability(governorIllBdaySex, (x: Boolean) => x))
    println("P(∀ n ∈  (1,N) · zₙ = GOVZIPCODE ∧ bₙ = GOVBDAY ∧ sₙ = GOVSEX  →  dₙ=Ill) = " +
      alg.probability(governorIllZipBdaySex, (x: Boolean) => x))


    println("\n\n//************Attribute Uniqueness Plots************//")
    PlotFigaro.plotElementIntProbabilities(alg,numGovZip,
      1, (0,50),
      "Number of rows with Governor's ZIP",
      "numgovzip.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovBday,
      1, (0,50),
      "Number of rows with Governor's BDay",
      "numgovbday.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovSex,
      1, (0,1000),
      "Number of rows with Governor's Sex",
      "numgovsex.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovZipSex,
      1, (0,50),
      "Number of rows with Governor's Zip and Sex",
      "numgovzipsex.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovZipBday,
      1, (0,50),
      "Number of rows with Governor's Zip and BDay",
      "numgovzipbday.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovBdaySex,
      1, (0,50),
      "Number of rows with Governor's BDay & Sex",
      "numgovbdaysex.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovZipBdaySex,
      1, (0,50),
      "Number of rows with Governor's Zip & BDay & Sex",
      "numgovzipbdaysex.html")

    // Sanity check to see the quality of the inferred distribution
    PlotFigaro.plotElementIntProbabilities(alg,secondRowBDay,
      1, (0,100),
      "Inferred bday",
      "secondRowBDay.html")

    // These plot are only correct if the condition is active
    println("\n\n//************Probability of Learning Plots************//")
    PlotFigaro.plotElementIntProbabilities(alg,numGovIllZip,
      1, (0,5),
      "Number of rows with Governor's ZIP that are not Ill (given that there are 5 rows with Governor's ZIP)",
      "numgovillzip.html")
    PlotFigaro.plotElementIntProbabilities(alg,numGovIllZipBdaySex,
      1, (0,50),
      "Number of rows with Governor's Zip & BDay & Sex and Ill (given that there are 5 rows with Governor's ZIP)",
      "numgovillzipbdaysex.html")


    // This plot is only correct if the condition is active
    // X-axis
    val x = (0 to 5)

    // Y-axis (Probabilities)
    val y = x.map{ (i) => alg.probability(numGovIllZip, (x: Int) => i == x)}

    // Plotting
    // Define plot
    val plot = Bar(
      x.map((x) => x*1.0/5.0),
      y
    )    
    val data = Seq(plot)

    //Layout
    val layout = Layout(
      title = "p(#(z = GOVZIP ∧ d = Ill) | #(z = GOVZIP) = 5)"
    )
    // Show plot
    Plotly.plot(
      "test.html",
      data,
      layout
    )


    alg.kill()
  }
}
