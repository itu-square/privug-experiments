import com.cra.figaro.language._
import com.cra.figaro.library.atomic._
import com.cra.figaro.algorithm.sampling._
import com.cra.figaro.algorithm.factored.DensityEstimator
import com.cra.figaro.library.collection.FixedSizeArray
import com.cra.figaro.library.collection.FixedSizeArrayElement
import com.cra.figaro.library.collection.VariableSizeArray

// Plotting
import plotly._, layout._, Plotly._
import com.cra.figaro.library.atomic.continuous.KernelDensity

object AverageAge {

  // Define alpha
  def alpha (records: Array[(String, Double)]) :Double =
    records
      .map { case (n,a) => (a)}
      .reduce { (x, y) => x + y} / records.length

  def alpha_p(records: FixedSizeArrayElement[(String, Double)]):
  Element[Double] = {
    val averageAge: Element[Double] = records
      .map { case (n, a) => (a, 1) }
      .reduce { case (x, y) => (x._1 + y._1, x._2 + y._2) }
      .map { case (sum, count) => sum / count }
    averageAge
  }


  def prior(size: Element[Int]): FixedSizeArrayElement[(String, Double)] = {

    val dict: Seq[String] = List("John", "Jho", "Joe", "Joo", "Hoj", "Haj",
      "Jah", "Jih", "Jee", "Chi", "Khi", "Kha", "Khe", "Kho", "Khu", "Anz",
      "Inz", "Enz", "Unz", "Ulu", "Ulo", "Ula", "Ule", "Uli", "Olo", "Ole",
      "Olu", "Oli", "Lli", "Lle", "Lla", "Llo", "Llu", "Lly", "Pia", "Pio",
      "Pie", "Pii", "Piu", "Piy", "Per", "Pit", "Pat", "Por", "Par", "Pur",
      "Pir", "Pyr", "Bob")
    VariableSizeArray(size, i =>
      for {
        n <- if (i==0) Constant ("Alice") else discrete.Uniform (dict: _*)
        // AverageAge prior (active only if running the average age running example)
        a <- if (i==0) continuous.Uniform(0.0,100.0) else continuous.Normal(55.2,3.5)//Math.pow(2,2))
        // DP prior (activate only if running DP.scala)
        // a <- if (i<5) continuous.Uniform(10.0, 200.0) else continuous.Uniform(80.0,90.0)//Constant(29.2)
      }
      yield  (n, a)
    )
  }

  def main(args: Array[String]) = {

    val t_init = System.nanoTime()
    // Attacker's belief before running the program
    val original = continuous.Uniform(0.0,100.0) // NOTE: Should be the same as in case i==0 in line 60
 
    // Prior distribution for the size of the database
    val sizeDatabase = discrete.Binomial(300, 0.3)
    val prior        = AverageAge.prior(sizeDatabase)
    val average      = AverageAge.alpha_p(prior)

    // Attacker's knowledge after observation
    average.setCondition ((x) => ((x >= 55.25) && (x < 55.35))) // Normal rest

    // The attacker considers that databases of size 3 to 5 are more
    // likely than the rest
    // sizeDatabase.setConstraint(x => if (3 <= x && x <= 5) 1.0 else 0.001)
    // sizeDatabase.setCondition(x => x==4)

    // Obtain Alice's age distribution
    val aliceRow = prior(0)
    val aliceAge = aliceRow.map( (x) => x._2)
    val diff     = original-aliceAge


    //Sampling
    val numberOfSamples = 10000
    val alg = Importance(numberOfSamples,
      original, average, aliceAge, sizeDatabase, diff
    )

    // Querying
    alg.start()

    // Comparing expected values and standard deviation
    val expBefore: Double = alg.expectation(original, (x: Double) => x)
    val expAfter: Double  = alg.expectation(aliceAge, (x: Double) => x)
    val sdBefore: Double = Math.sqrt(alg.variance(original))
    val sdAfter: Double  = Math.sqrt(alg.variance(aliceAge))
    println("\nExpectation of Alice's before observation: "+ expBefore + "\n" +
            "Standard deviation: " + sdBefore)
    println("\nExpectation of Alice's after observation: "+ expAfter + "\n" +
            "Standard deviation: " + sdAfter)
    println("\nDifference expected value: "+ math.abs(expBefore - expAfter))
    println("Difference standard deviation: "+ math.abs(sdBefore - sdAfter))

    println("\nExpected value average: " + alg.expectation(average, (x: Double) => x))
    println("Standard deviation average: " + Math.sqrt(alg.variance(average)) + "\n")

    // Compare probabilities of an interesting predicate
    println("Pr(age < 18) = " + alg.probability(original, (a: Double) => a < 18.0) + " before observation")
    println("Pr(age < 18) = " + alg.probability(aliceAge, (a: Double) => a < 18.0) + " after observation")

    // Probabilities greater than 0
    println("Pr(diff ≥ 0) = " + alg.probability(diff, (a: Double) => a >= 0))
    println("Pr(diff < 0) = " + alg.probability(diff, (a: Double) => a < 0))

    // Compare entropy
    println("Entropy aliceAge (prior): " + entropy(alg,original))
    println("Entropy aliceAge (posterior): " + entropy(alg,aliceAge))
    println("Entropy average: " + entropy(alg,average))

    // KL-divergence continuous
    println("KL(original|posterior)"+continuousKullbackLeiblerDistance(alg,original,aliceAge,numberOfSamples))

    // Plotting
    val plot: Boolean = true
    if (plot) {
      plot_kde(alg, average, 10, (0,100), "Average KDE", "average-kde.html")
      plot_kde(alg, aliceAge, 10, (0,100), "Alice Age KDE", "alice-age-kde.html")
      plot_kde(alg, diff, 10, (-100,100), "Diff prior/posterior Alice Age", "diff-kde.html")
      // plot_kde_int(alg, sizeDatabase, 1, (0,200), "Database Size KDE", "database-size-kde.html")

      val (x1,y1) = computeXY_kde(alg, aliceAge, 10, (0,100))
      val (x2,y2) = computeXY_kde(alg, original, 10, (0,100))

      val trace1 = Scatter(
        x1,
        y1,
        mode=element.ScatterMode(element.ScatterMode.Lines),
        name="Posterior"
      )
      val trace2 = Scatter(
        x2,
        y2,
        mode=element.ScatterMode(element.ScatterMode.Lines),
        name="Prior"
      )
      val data = Seq(trace1,trace2)
      val layout = Layout(
        title = "Alice age Prior/Posterior"
      )
      Plotly.plot(
        "alice_age.html",
        data,
        layout
      )
    }

    // Clean up
    alg.kill()
    println("\nExecution time (ns): " + (System.nanoTime()-t_init))
  }

  /******************************/
  /*** Plotting Probabilities ***/
  /******************************/

  /**
    * A method to abstract away the details of plotting
    * 
    * @param alg The sampling algorithm to use (for now only
    * `Importance`)
    * @param dist The distribution to plot
    * @param precision The precision of the plotting, e.g., 10 plots
    * with one decimal 0.1, 0.2, 0.3, ...
    * @param range A pair indicating the first and last element of the
    * x-axis
    * @param title Title of the plot
    * @param filename filename of the generated html file
    */
  def plot(alg: Importance, dist: Element[Double], precision: Int, range: (Int, Int), title: String, filename: String) {
    // Variables
    val x_0: Int = range._1 * precision
    val x_n: Int = range._2 * precision
    val delta: Double = 1.0 / precision

    // X-axis
    val x = (x_0 to x_n).map { (i: Int) => i / (1.0*precision)}

    // Y-axis (Probabilities)
    val y = x.map{ (i) => alg.probability(dist, (x: Double) => i <= x && x < i + delta)}

    // Plotting
    // Define plot
    val plot = Scatter(
      x,
      y,
      mode=element.ScatterMode(element.ScatterMode.Lines)
    )
    val data = Seq(plot)

    //Layout
    val layout = Layout(
      title = title
    )
    // Show plot
    Plotly.plot(
      filename,
      data,
      layout
    )
  }

  def plotInt(alg: Importance, dist: Element[Int], precision: Int, range: (Int, Int), title: String, filename: String) {
    // Variables
    val x_0: Int = range._1 * precision
    val x_n: Int = range._2 * precision
    val delta: Double = 1.0 / precision

    // X-axis
    val x = (x_0 to x_n).map { (i: Int) => i / (1.0*precision)}

    // Y-axis (Probabilities)
    val y = x.map{ (i) => alg.probability(dist, (x: Int) => i <= x && x < i + delta)}

    // Plotting
    // Define plot
    val plot = Scatter(
      x,
      y,
      mode=element.ScatterMode(element.ScatterMode.Lines)
    )    
    val data = Seq(plot)

    //Layout
    val layout = Layout(
      title = title
    )
    // Show plot
    Plotly.plot(
      filename,
      data,
      layout
    )

  }

  def computeXY(alg: Importance, dist: Element[Double], precision: Int, range: (Int, Int)):
      (IndexedSeq[Double], IndexedSeq[Double]) = {
    // Variables
    val x_0: Int = range._1 * precision
    val x_n: Int = range._2 * precision
    val delta: Double = 1.0 / precision

    // X-axis
    val x = (x_0 to x_n).map { (i: Int) => i / (1.0*precision)}

    // Y-axis (Probabilities)
    val y = x.map{ (i) => alg.probability(dist, (x: Double) => i < x && x <= i + delta)}

    return (x,y)
  }

  /********************/
  /*** Plotting kde ***/
  /********************/

  def plot_kde(alg: Importance, dist: Element[Double],
    precision: Int, range: (Int, Int),
    title: String, filename: String) {

    // Variables
    val x_0: Int = range._1 * precision
    val x_n: Int = range._2 * precision
    val delta: Double = 1.0 / precision

    // X-axis
    val x = (x_0 to x_n).map { (i: Int) => i / (1.0*precision)}

    // Y-axis (Probabilities)
    val samples = alg.computeDistribution(dist)
    val samples_prime = samples map {_._2}

    // Compute bandwith (as described in
    // https://en.wikipedia.org/wiki/Kernel_density_estimation#A_rule-of-thumb_bandwidth_estimator)
    val sd: Double = Math.sqrt(alg.variance(dist))
    val bandwith: Double = Math.pow((4*Math.pow(sd, 5))/(3*samples_prime.size), 0.2)  // 1/5

    val kde = KernelDensity(samples_prime, bandwith)
    val y = x.map{ (i) => kde.density(i)}

    // Plotting
    // Define plot
    val plot = Scatter(
      x,
      y,
      mode=element.ScatterMode(element.ScatterMode.Lines)
    )
    val data = Seq(plot)

    //Layout
    val layout = Layout(
      title = title
    )
    // Show plot
    Plotly.plot(
      filename,
      data,
      layout
    )
  }

  def plot_kde_int(alg: Importance, dist: Element[Int],
    precision: Int, range: (Int, Int),
    title: String, filename: String) {
    // Variables
    val x_0: Int = range._1 * precision
    val x_n: Int = range._2 * precision
    val delta: Double = 1.0 / precision

    // X-axis
    val x = (x_0 to x_n).map { (i: Int) => i / (1.0*precision)}

    // Y-axis (Probabilities)
    val samples = alg.computeDistribution(dist)
    val samples_prime = samples map {(x) => x._2 * 1.0}
    val kde = KernelDensity(samples_prime, 5.0) // Bandwith? 
    val y = x.map{ (i) => kde.density(i)}

    // Plotting
    // Define plot
    val plot = Scatter(
      x,
      y,
      mode=element.ScatterMode(element.ScatterMode.Lines)
    )
    val data = Seq(plot)

    //Layout
    val layout = Layout(
      title = title
    )
    // Show plot
    Plotly.plot(
      filename,
      data,
      layout
    )
  }

  def computeXY_kde(alg: Importance, dist: Element[Double], precision: Int, range: (Int, Int)):
      (IndexedSeq[Double], IndexedSeq[Double]) = {
    // Variables
    val x_0: Int = range._1 * precision
    val x_n: Int = range._2 * precision
    val delta: Double = 1.0 / precision

    // X-axis
    val x = (x_0 to x_n).map { (i: Int) => i / (1.0*precision)}

    // Y-axis
    val samples = alg.computeDistribution(dist)
    val samples_prime = samples map {_._2}

    // Compute bandwith (as described in
    // https://en.wikipedia.org/wiki/Kernel_density_estimation#A_rule-of-thumb_bandwidth_estimator)
    val sd: Double = Math.sqrt(alg.variance(dist))
    val bandwith: Double = Math.pow((4*Math.pow(sd, 5))/(3*samples_prime.size), 0.2)

    val kde = KernelDensity(samples_prime, bandwith)
    val y = x.map{ (i) => kde.density(i)}

    return (x,y)
  }


  // For convenience to use log base 2
  def log2(x: Double): Double = {
    return Math.log10(x)/Math.log10(2.0)
  }

  /**
    * Implementation of the entropy function in
    * "A nonparametric estimation of the entropy for absolutely continuous distributions"
    * https://ieeexplore.ieee.org/abstract/document/1055550
    * (It is an old estimator, but it is easy to implement.)
    * 
    * It works for continious distributions only. If the distribution
    * is discrete, it is better to use Shannon's original definition.
    * 
    */ 
  def entropy(alg: Importance, dist: Element[Double]): Double = {

    val samples = alg.computeDistribution(dist).map{_._2}
    val sd: Double = Math.sqrt(alg.variance(dist))
    val bandwith: Double = Math.pow((4*Math.pow(sd, 5))/(3*samples.size), 0.2)
    val kde = KernelDensity(samples, bandwith)
    val samples_densities = samples.map{(s: Double) => kde.density(s)}

    // val (x,y) = computeXY_kde(alg, dist, precision, range)
    return samples_densities.map {
      (d: Double) => if (d > 0) log2(d) else 0 // x < 0 cannot happen (due to Kernel density), x = 0 should be 0
    }.reduce{ (x: Double, y: Double) => x+y } * (-1.0/samples_densities.size)
  }


  // Attempt to implement Hellinger Distance to compare distributions
  // NOTE: Cheating a bit. We are in a continuous domain, and we use
  // the version for discrete distributions
  def discreteHellingerDistance(l1: Stream[(Double, Double)], l2: Stream[(Double, Double)]): Double = {
    val x: Stream[Double] = for {
      i1 <- l1
      i2 <- l2
    } yield math.pow(math.sqrt(i1._1)-math.sqrt(i2._1), 2) // (sqrt(p_i) - sqrt(q_i))^2
    math.sqrt(x.reduce((x,y) => x+y)) * (1 / math.sqrt(2))
  }

  /** 
    * It computes D_{KL}(l1 || l2).
    * The implementation is for discrete distributions.
    * For now it runs out of memory for large sampling.
    */
  def discreteKullbackLeiblerDistance(l1: Seq[Double], l2: Seq[Double]): Double = {
    // Remove zeros from denominator
    val x: Seq[Double] = for {
      p <- l1
      q <- l2
    } yield { if (p == 0 && q == 0) {0}
         else if (p == 0 && q != 0) {0}
         else if (p != 0 && q == 0) {Double.PositiveInfinity}
         else {p*(math.log(p/q))}
    } // P(x) log(P(x)/Q(x))
    x.reduce((x,y) => x+y)
  }

  def continuousKullbackLeiblerDistance(alg: Importance,
    p: Element[Double], q: Element[Double],
    numberOfSamples: Int): Double = {
    //Attemp to approximate KL from samples (based on algorithm A in information theory paper)
    val p_samples = alg.distribution(p).map{_._2}
    val ordered_q_samples = alg.distribution(q) // Get samples 
                        .map{_._2} // Remove marginal probabilities
                        .sortWith((x,y) => x <= y) // Order samples
                        .zipWithIndex // Attach index
    val l_m = 100
    val filtered_ys = ordered_q_samples.par.filter(_._2%l_m == 0)

    val ks = filtered_ys.par.map{ (z) =>
      p_samples.par.filter( (x) =>
        ordered_q_samples.par.find((y) => y._2 == (z._2 - l_m)).getOrElse((101.0,0))._1 <= x &&
          x < z._1
      ).size
    }.filter( _ != 0) // Removing 0 for now, i.e., first interval
    val n = numberOfSamples
    val m = numberOfSamples
    val k_Tm = ks(ks.size-1)
    val result: Double = // Ignoring first and last intervals for simplicity
      ks.par.map((k_i) =>
        (k_i*1.0/n*1.0)*(log2((k_i*1.0/n*1.0)/(l_m*1.0/m*1.0)))
      ).reduce((x: Double, y: Double) => x+y)
    println("Estimated KL: " + result)
    return result
  }
}
