import com.cra.figaro.language._
import com.cra.figaro.library.atomic._
import com.cra.figaro.algorithm.sampling._
import com.cra.figaro.algorithm.factored.DensityEstimator
import com.cra.figaro.library.collection.FixedSizeArray
import com.cra.figaro.library.collection.FixedSizeArrayElement
import com.cra.figaro.library.collection.VariableSizeArray
import com.cra.figaro.library.atomic.discrete
import com.cra.figaro.library.atomic.discrete._
import com.cra.figaro.library.compound._

// Plotting
import plotly._, layout._, Plotly._
import com.cra.figaro.library.atomic.continuous.KernelDensity

// Running shell commands
import sys.process._

// Writing in CSV files
import scala.collection.JavaConversions._
import au.com.bytecode.opencsv.CSVWriter
import scala.collection.mutable.ListBuffer
import java.io.{BufferedWriter, FileWriter}

object DP {

  def dp_average(N: Integer, epsilon: Double, records: FixedSizeArrayElement[(String, Double)]):
      Element[Double] = {

    val delta = Constant(100.0)/records.length.map{(x: Int) => x*1.0} // because the minimum age is 0 and maximum 100
    println("delta: "+delta)

    val lambda = Constant(epsilon)/delta // b=delta/epsilon and b=1/lambda
    val X = continuous.Exponential(lambda) // Exponential(\lambda)
    val Y = Flip(0.5) // Bernoulli(0.5)

    // ref: https://en.wikipedia.org/wiki/Laplace_distribution
    // If X ~ Exponential(\lambda) and Y ~ Bernoulli(0.5), then X(2Y-1) ~ Laplace(0,\lambda^-1)
    val laplaceNoise = If(Y, X*Constant(-1.0), X) // Laplace(0,b=lambda^-1) | Laplace(0,b=\delta/\epsilon)

    println("noise: Laplace(b=delta/epsilon)")
    //return Apply(AverageAge.alpha_p(records), laplaceNoise, (ro: Double, noise: Double) => ro+noise)
    return AverageAge.alpha_p(records) ++ laplaceNoise
  }

  def main(args: Array[String]) = {
    println("### Experiment Differential Privacy | Configuration ###")

    val N = 200
    val dbSize = Constant(N)
    println("Database size: Constant("+N+")")

    val prior  = AverageAge.prior(dbSize) // Prior defined in the AverageAge class
    println("Secret prior (5 ppl): Uniform(10,200)")
    println("Others prior (195 ppl): Uniform(80.0,90.0)")

    // Differential Privacy noise
    val epsilon = 0.5 // Change to evaluate other values
    println("epsilon: "+epsilon)

    // Execute the program probabilistically
    println("noise: Laplace(b=delta/epsilon)")
    val output: Element[Double] = dp_average(N, epsilon, prior)

    // Set experiment's condition (optional)
    // output.setCondition((o) => (44 < o) && (o <= 45))
    // println("Condition: 44 < output < 45")

    // Secret
    val aliceRow = prior(0)
    val secret = aliceRow.map( (x) => x._2)

    val numSamples = 10000
    println("Number of samples (for IS): "+numSamples)
    val alg = Importance(numSamples, secret, output)

    alg.start()

    println("\n### Experiment Differential Privacy | Query Results ###")

    // We use our own implementation of the entropy estimator defined in the AverageAge class
    val h_secret = AverageAge.entropy(alg,secret)
    val h_output = AverageAge.entropy(alg,output)
    println("H(secret) = " + h_secret)
    println("H(output) = " + h_output)

    // We use an external python library for Mutual Information
    println("I(secret|output) = "+mutualInformation(alg,secret,output))


    PlotFigaro.plotKDE(alg, output, 10, (-10,100), "Output", "output.html")
    PlotFigaro.plotKDE(alg, secret, 10, (0,100), "Secret", "secret.html")
  }

  // We use an external python library for Mutual Information
  def mutualInformation(alg: Importance, p: Element[Double], q: Element[Double]): Double = {

    val xx: List[Double] = alg.computeDistribution(p).toList.map{x => x._2}
    val yy: List[Double] = alg.computeDistribution(q).toList.map{x => x._2}

    val outputFile = new BufferedWriter(new FileWriter("temp.csv"))
    val csvWriter = new CSVWriter(outputFile)
    val csvFields = Array("x", "y")

    val nameList = xx
    val ageList = yy
    val joint = xx zip yy

    var listOfRecords = new ListBuffer[Array[String]]()
    listOfRecords += csvFields
    joint map {
      case (x,y) => listOfRecords += Array(x.toString,y.toString)
    }
    csvWriter.writeAll(listOfRecords.toList)
    outputFile.close()

    val cmd: String = "python3 -W ignore scripts/NPEET/npeet/mutual_info.py"
    val result: String = cmd.!!
    return result.toDouble
  }

  def entropy(alg: Importance, p: Element[Double]): Double = {

    val x: String = alg.computeDistribution(p).toList.map{x => x._2+","}.foldLeft("")(_+_).dropRight(1)

    val cmd: String = "python3 scripts/NPEET/npeet/entropy.py " + x
    val result: String = cmd.!!
    return result.toDouble
  }

}
