# Implementation of the Privug method in Figaro

Implementation of the privug method [1] in the probabilistic
programming framework [Figaro](https://github.com/p2t2/figaro).

The repository contains 4 experiments located in the following files:

- `AverageAge.scala` contains the average age running example program
  in [1]. Additionally, it includes our own implementation of
  differential entropy [2] and KL-divergence [3] estimators.
  
- `DP.scala` contains the implementation and analysis of differential
  privacy program.
  
- `Governor.scala` contains the implementation and analysis of
  Governor program with naive anonymization.
  
- `GovernorKAnonymity.scala` contains the implementation and analysis
  of Governor program with k-anonymity.

We recommend running the programs using the Simple Build Tool (SBT).
We use Figaro 5 which requires SBT version 0.13.16 or later.  The
documentation for SBT and installation guide is available at:
http://www.scala-sbt.org/0.13/tutorial/index.html. To run an
experiment, edit the file `build.sbt` by replacing the value of the
variable `mainClassName`. For instance, to run the average age
experiment set `val mainClassName: String = "AverageAge"`.



## References

- [1] Raúl Pardo, Willard Rafnsson, Christian Probs and Andrzej
  Waswoski. Privug: Quantifying Leakage using Probabilistic
  Programming for Privacy Risk Analysis. arXiv preprint
  arXiv:2011.08742 (2020). https://arxiv.org/abs/2011.08742
  
- [2] Ibrahim A. Ahmad and Pi-Erh Lin. A nonparametric estimation of
  the entropy for absolutely continuous distributions (corresp.). IEEE
  Trans. Inf. Theory, 22(3):372–375, 1976.
  
- [3] Qing Wang, Sanjeev R. Kulkarni, and Sergio Verdú. Divergence
  estimation of continuous distributions based on data- dependent
  partitions. IEEE Trans. Inf. Theory, 51(9):3064–3074, 2005.
