import sys
import numpy as np
import entropy_estimators as ee

param1 = np.array(sys.argv[1].split(','),dtype='float64')
print(ee.entropy([[i] for i in param1]))
