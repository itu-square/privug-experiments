import numpy as np
import pandas as pd
from sklearn.feature_selection import f_regression, mutual_info_regression

df = pd.read_csv('temp.csv')
param1 = np.array([np.array([i],dtype='float64') for i in df['x']])
param2 = np.array([np.array([i],dtype='float64') for i in df['y']])
print(mutual_info_regression(param1,param2,discrete_features=False)[0])

