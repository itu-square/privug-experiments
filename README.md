# Privug Experiments

This repository contains the complete set of experiments and
evaluation of the privacy risk analysis method _privug_; introduced in
[1]. This method uses probabilistic programming frameworks to quantify
privacy risks. We provide two instantiations of privug in:
[Figaro](https://github.com/p2t2/figaro)/Scala and
[PyMC3](https://docs.pymc.io/)/Python. The content of the repository
is split in two sections, one for each instantiation of privug:

- `privug-figaro` contains the complete Figaro/Scala implementation of
  the programs and analyses for the average age running example, and
  the differential privacy, naive anonymization and k-anonymity
  anonymization systems.

- `privug-pycm3` contains the complete implementation PyMC3/Python
  experiments of accuracy evaluation, scalability evaluation, and
  comparison with
  [LeakWatch](https://www.cs.bham.ac.uk/research/projects/infotools/leakwatch/). It
  also contains the differential privacy case study implemented in
  PyMC3. Furthermore, we include an experiment showing the use of
  `pymc3-privug` to analyze the 3rd party anonymization library
  [OpenDP](https://github.com/opendp). The repository contains an
  experiment analyzing the result of a program using OpenDP to add
  Laplacian noise.


## References

- [1] Raúl Pardo, Willard Rafnsson, Christian Probs and Andrzej
  Waswoski. Privug: Quantifying Leakage using Probabilistic
  Programming for Privacy Risk Analysis. arXiv preprint
  arXiv:2011.08742 (2020). https://arxiv.org/abs/2011.08742
